From b7a6c0a7ee8e66c5daa377d2e6f59461ed34f3bf Mon Sep 17 00:00:00 2001
From: Gary Lin <glin@suse.com>
Date: Tue, 25 May 2021 12:46:03 +0200
Subject: [PATCH] mokutil: remove "--simple-hash"

The simple-hash password format is used by the very early MokManager and
not the default format anymore after we changed to password-crypt.
Remove the code to reduce the code size.

Signed-off-by: Gary Lin <glin@suse.com>
---
 src/mokutil.c | 87 +++++----------------------------------------------
 1 file changed, 7 insertions(+), 80 deletions(-)

diff --git a/src/mokutil.c b/src/mokutil.c
index 252dc7a327f..563e585979b 100644
--- a/src/mokutil.c
+++ b/src/mokutil.c
@@ -76,7 +76,6 @@
 #define TEST_KEY           (1 << 14)
 #define RESET              (1 << 15)
 #define GENERATE_PW_HASH   (1 << 16)
-#define SIMPLE_HASH        (1 << 17)
 #define IGNORE_DB          (1 << 18)
 #define USE_DB             (1 << 19)
 #define MOKX               (1 << 20)
@@ -95,8 +94,6 @@ typedef unsigned long efi_status_t;
 typedef uint8_t  efi_bool_t;
 typedef wchar_t efi_char16_t;		/* UNICODE character */
 
-static int use_simple_hash;
-
 typedef enum {
 	DELETE_MOK = 0,
 	ENROLL_MOK,
@@ -182,7 +179,6 @@ print_help ()
 	printf ("Supplimentary Options:\n");
 	printf ("  --hash-file <hash file>\t\tUse the specific password hash\n");
 	printf ("  --root-pw\t\t\t\tUse the root password\n");
-	printf ("  --simple-hash\t\t\t\tUse the old password hash method\n");
 	printf ("  --mokx\t\t\t\tManipulate the MOK blacklist\n");
 }
 
@@ -814,32 +810,6 @@ error:
 	return ret;
 }
 
-static int
-generate_auth (void *new_list, int list_len, char *password,
-	       unsigned int pw_len, uint8_t *auth)
-{
-	efi_char16_t efichar_pass[PASSWORD_MAX+1];
-	unsigned long efichar_len;
-	SHA256_CTX ctx;
-
-	if (!password || !auth)
-		return -1;
-
-	efichar_len = efichar_from_char (efichar_pass, password,
-					 pw_len * sizeof(efi_char16_t));
-
-	SHA256_Init (&ctx);
-
-	if (new_list)
-		SHA256_Update (&ctx, new_list, list_len);
-
-	SHA256_Update (&ctx, efichar_pass, efichar_len);
-
-	SHA256_Final (auth, &ctx);
-
-	return 0;
-}
-
 static void
 generate_salt (char salt[], unsigned int salt_size)
 {
@@ -979,7 +949,6 @@ update_request (void *new_list, int list_len, MokRequest req,
 	size_t data_size;
 	const char *req_name, *auth_name;
 	pw_crypt_t pw_crypt;
-	uint8_t auth[SHA256_DIGEST_LENGTH];
 	char *password = NULL;
 	unsigned int pw_len;
 	int auth_ret;
@@ -1028,12 +997,7 @@ update_request (void *new_list, int list_len, MokRequest req,
 			goto error;
 		}
 
-		if (!use_simple_hash) {
-			auth_ret = generate_hash (&pw_crypt, password, pw_len);
-		} else {
-			auth_ret = generate_auth (new_list, list_len, password,
-						  pw_len, auth);
-		}
+		auth_ret = generate_hash (&pw_crypt, password, pw_len);
 		if (auth_ret < 0) {
 			fprintf (stderr, "Couldn't generate hash\n");
 			goto error;
@@ -1069,13 +1033,8 @@ update_request (void *new_list, int list_len, MokRequest req,
 	}
 
 	/* Write MokAuth, MokDelAuth, MokXAuth, or MokXDelAuth */
-	if (!use_simple_hash) {
-		data = (void *)&pw_crypt;
-		data_size = PASSWORD_CRYPT_SIZE;
-	} else {
-		data = (void *)auth;
-		data_size = SHA256_DIGEST_LENGTH;
-	}
+	data = (void *)&pw_crypt;
+	data_size = PASSWORD_CRYPT_SIZE;
 
 	if (efi_set_variable (efi_guid_shim, auth_name, data, data_size,
 			      attributes, S_IRUSR | S_IWUSR) < 0) {
@@ -1904,26 +1863,16 @@ set_password (const char *hash_file, const int root_pw, const int clear)
 			goto error;
 		}
 
-		if (!use_simple_hash) {
-			pw_crypt.method = DEFAULT_CRYPT_METHOD;
-			auth_ret = generate_hash (&pw_crypt, password, pw_len);
-		} else {
-			auth_ret = generate_auth (NULL, 0, password, pw_len,
-						  auth);
-		}
+		pw_crypt.method = DEFAULT_CRYPT_METHOD;
+		auth_ret = generate_hash (&pw_crypt, password, pw_len);
 		if (auth_ret < 0) {
 			fprintf (stderr, "Couldn't generate hash\n");
 			goto error;
 		}
 	}
 
-	if (!use_simple_hash) {
-		data = (void *)&pw_crypt;
-		data_size = PASSWORD_CRYPT_SIZE;
-	} else {
-		data = (void *)auth;
-		data_size = SHA256_DIGEST_LENGTH;
-	}
+	data = (void *)auth;
+	data_size = SHA256_DIGEST_LENGTH;
 	uint32_t attributes = EFI_VARIABLE_NON_VOLATILE
 			      | EFI_VARIABLE_BOOTSERVICE_ACCESS
 			      | EFI_VARIABLE_RUNTIME_ACCESS;
@@ -2301,8 +2250,6 @@ main (int argc, char *argv[])
 	DBName db_name = MOK_LIST_RT;
 	int ret = -1;
 
-	use_simple_hash = 0;
-
 	if (!efi_variables_supported ()) {
 		fprintf (stderr, "EFI variables are not supported on this system\n");
 		exit (1);
@@ -2329,7 +2276,6 @@ main (int argc, char *argv[])
 			{"hash-file",          required_argument, 0, 'f'},
 			{"generate-hash",      optional_argument, 0, 'g'},
 			{"root-pw",            no_argument,       0, 'P'},
-			{"simple-hash",        no_argument,       0, 's'},
 			{"ignore-db",          no_argument,       0, 0  },
 			{"use-db",             no_argument,       0, 0  },
 			{"mok",                no_argument,       0, 'm'},
@@ -2531,10 +2477,6 @@ main (int argc, char *argv[])
 		case 'x':
 			command |= EXPORT;
 			break;
-		case 's':
-			command |= SIMPLE_HASH;
-			use_simple_hash = 1;
-			break;
 		case 'm':
 			db_name = MOK_LIST_RT;
 			break;
@@ -2555,9 +2497,6 @@ main (int argc, char *argv[])
 		}
 	}
 
-	if (use_root_pw == 1 && use_simple_hash == 1)
-		use_simple_hash = 0;
-
 	if (hash_file && use_root_pw)
 		command |= HELP;
 
@@ -2593,22 +2532,18 @@ main (int argc, char *argv[])
 			ret = list_keys_in_var ("MokDel", efi_guid_shim);
 			break;
 		case IMPORT:
-		case IMPORT | SIMPLE_HASH:
 			ret = issue_mok_request (files, total, ENROLL_MOK,
 						 hash_file, use_root_pw);
 			break;
 		case DELETE:
-		case DELETE | SIMPLE_HASH:
 			ret = issue_mok_request (files, total, DELETE_MOK,
 						 hash_file, use_root_pw);
 			break;
 		case IMPORT_HASH:
-		case IMPORT_HASH | SIMPLE_HASH:
 			ret = issue_hash_request (hash_str, ENROLL_MOK,
 						  hash_file, use_root_pw);
 			break;
 		case DELETE_HASH:
-		case DELETE_HASH | SIMPLE_HASH:
 			ret = issue_hash_request (hash_str, DELETE_MOK,
 						  hash_file, use_root_pw);
 			break;
@@ -2623,11 +2558,9 @@ main (int argc, char *argv[])
 			ret = export_db_keys (db_name);
 			break;
 		case PASSWORD:
-		case PASSWORD | SIMPLE_HASH:
 			ret = set_password (hash_file, use_root_pw, 0);
 			break;
 		case CLEAR_PASSWORD:
-		case CLEAR_PASSWORD | SIMPLE_HASH:
 			ret = set_password (NULL, 0, 1);
 			break;
 		case DISABLE_VALIDATION:
@@ -2643,7 +2576,6 @@ main (int argc, char *argv[])
 			ret = test_key (ENROLL_MOK, key_file);
 			break;
 		case RESET:
-		case RESET | SIMPLE_HASH:
 			ret = reset_moks (ENROLL_MOK, hash_file, use_root_pw);
 			break;
 		case GENERATE_PW_HASH:
@@ -2662,22 +2594,18 @@ main (int argc, char *argv[])
 			ret = list_keys_in_var ("MokXDel", efi_guid_shim);
 			break;
 		case IMPORT | MOKX:
-		case IMPORT | SIMPLE_HASH | MOKX:
 			ret = issue_mok_request (files, total, ENROLL_BLACKLIST,
 						 hash_file, use_root_pw);
 			break;
 		case DELETE | MOKX:
-		case DELETE | SIMPLE_HASH | MOKX:
 			ret = issue_mok_request (files, total, DELETE_BLACKLIST,
 						 hash_file, use_root_pw);
 			break;
 		case IMPORT_HASH | MOKX:
-		case IMPORT_HASH | SIMPLE_HASH | MOKX:
 			ret = issue_hash_request (hash_str, ENROLL_BLACKLIST,
 						  hash_file, use_root_pw);
 			break;
 		case DELETE_HASH | MOKX:
-		case DELETE_HASH | SIMPLE_HASH | MOKX:
 			ret = issue_hash_request (hash_str, DELETE_BLACKLIST,
 						  hash_file, use_root_pw);
 			break;
@@ -2688,7 +2616,6 @@ main (int argc, char *argv[])
 			ret = revoke_request (DELETE_BLACKLIST);
 			break;
 		case RESET | MOKX:
-		case RESET | SIMPLE_HASH | MOKX:
 			ret = reset_moks (ENROLL_BLACKLIST, hash_file, use_root_pw);
 			break;
 		case TEST_KEY | MOKX:
-- 
2.31.1

